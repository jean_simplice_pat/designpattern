package com.leretvil.bridge.colorOfshape;

public class Rectangle extends Shape {

    public Rectangle(Color color) {
        super(color);
    }

    @Override
    public void applyColor() {
        System.out.print("Rectangle with color:");
        color.applyColor();
    }
}
