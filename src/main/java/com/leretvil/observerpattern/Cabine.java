package com.leretvil.observerpattern;

public class Cabine implements Observateur {
    @Override
    public void notifier() {
        System.out.println("Cabine notifié");
    }
}
