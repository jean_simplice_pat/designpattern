package com.leretvil.AbstractFactory.bombed;

import com.leretvil.AbstractFactory.Room;

public class RoomWithBom extends Room {

    private BombedWall wall;
    private boolean withBomb;

    public RoomWithBom() {
    }

    public RoomWithBom(int n) {
        super(n);
    }

    public RoomWithBom(BombedWall wall, boolean withBomb) {
        this.wall = wall;
        this.withBomb = withBomb;
    }
}
