package server;

import java.io.PrintWriter;

public class ProducerServerManager extends ServerManager {

    public ProducerServerManager(Buffer buffer, int portNumber) {
        this.buffer = buffer;
        this.portNumber = portNumber;
    }

    @Override
    boolean canDoProcess() {
        return !buffer.isFull();
    }

    @Override
    String process(String message, PrintWriter socketBufferedReader) {
        buffer.produce(message);
        socketBufferedReader.println(FIN);
        return FIN;
    }


}
