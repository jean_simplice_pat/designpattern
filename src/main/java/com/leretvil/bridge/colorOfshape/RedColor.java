package com.leretvil.bridge.colorOfshape;

public class RedColor implements Color {

    @Override
    public void applyColor() {
        System.out.println(" Red!");
    }
}
