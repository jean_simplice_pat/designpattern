package server;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server {
	
	public static void main(String[] args) {
		int consumerPort = 4030,
			producerPort = 4031;
		try{
		    int max = Integer.parseInt(args[0]);
		    Buffer tamp = new Buffer(max);
			ExecutorService executorService = Executors.newFixedThreadPool(2);
			executorService.execute(() -> new ProducerServerManager(tamp, producerPort).manage());
			executorService.execute(() -> new ConsumerServerManager(tamp, consumerPort).manage());
		} catch (Exception e) {
		    System.err.println("Error : " +e);
		}
	}
}
