package algorithms.impl.fly;

import algorithms.api.fly.FlyBehavior;

public class FlyRocketPowered implements FlyBehavior {
    public void fly() {
        System.out.println("I'm flying with rocket!");
    }
}
