package ducks;

import algorithms.api.fly.FlyBehavior;
import algorithms.api.quack.QuackBehavior;

public class DuckCall {
    QuackBehavior quackBehavior;

    public DuckCall() {
    }

    public void setQuackBehavior(QuackBehavior quackBehavior) {
        this.quackBehavior = quackBehavior;
    }

    public void performQuack() {
        quackBehavior.quack();
    }
}
