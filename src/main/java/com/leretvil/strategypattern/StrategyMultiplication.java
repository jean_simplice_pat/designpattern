package com.leretvil.strategypattern;

public class StrategyMultiplication implements Strategy{
    @Override
    public int execute(int a, int b) {
        return a*b;
    }
}
