package com.leretvil.bridge.colorOfshape;

public interface Color {

    void applyColor();
}
