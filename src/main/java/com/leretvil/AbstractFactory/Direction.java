package com.leretvil.AbstractFactory;

public enum Direction {
    NORTH, SOUTH, WEST, EAST
}
