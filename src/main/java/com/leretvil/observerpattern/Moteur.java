package com.leretvil.observerpattern;

public class Moteur implements Observateur {
    @Override
    public void notifier() {
        System.out.println("Moteur notifié");
    }
}
