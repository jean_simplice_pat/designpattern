package consumers;

import java.io.*;
import java.net.*;

public class ConsumerClient {
	public static final String SYN = "SYN";
	public static final String ACK = "ACK";
	public static final String CONSUME = "CONSUME";

	public static void main (String[] args) {
		int port= 4030;
		InetAddress hote=null;
		try{
			if (args.length>=2){
				hote= InetAddress.getByName(args[0]);
				port= Integer.parseInt(args[1]);
			}
			else{
				hote = InetAddress.getLocalHost();
			}
		}
		catch(UnknownHostException e){
			System.err.println("Machine inconnue :" +e);
		}
		try (Socket socket = new Socket(hote, port); BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream())); PrintWriter printWriter = new PrintWriter(socket.getOutputStream(), true)) {

			printWriter.println(SYN);
			System.out.println("Sent <" + SYN + "> to server");
			String responseMsg = bufferedReader.readLine();
			System.out.println("Server respond with <" + responseMsg + ">");
			if (ACK.equalsIgnoreCase(responseMsg)) {
				System.out.println("Sending consume request...");
				printWriter.println(CONSUME);
				responseMsg = bufferedReader.readLine();
				System.out.println("Consume response: " + responseMsg);
			} else {
				System.out.println("Cannot consume");
			}
		} catch (IOException e) {
			System.err.println("Error establishing connection : " + e);
		}
	}

}
