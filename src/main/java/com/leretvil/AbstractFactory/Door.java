package com.leretvil.AbstractFactory;

public class Door implements MapSite{
    public Door(Room room1, Room room2) {
        this.room1 = room1;
        this.room2 = room2;
    }

    Room otherSideRoom(Room room){
        return null;
    }

    @Override
    public void enter() {

    }

    private Room room1;
    private Room room2;
    private boolean isOpen;
}
