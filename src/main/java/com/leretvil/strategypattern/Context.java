package com.leretvil.strategypattern;


public class Context {

    Strategy strategyAddition;

    public Context() {
    }

    public Strategy getStrategyAddition() {
        return strategyAddition;
    }

    public void setStrategyAddition(Strategy strategyAddition) {
        this.strategyAddition = strategyAddition;
    }
}
