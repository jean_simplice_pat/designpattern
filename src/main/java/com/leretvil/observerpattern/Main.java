package com.leretvil.observerpattern;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        Cabine cabine = new Cabine();
        Moteur moteur = new Moteur();

        CapteurLaser laser = new CapteurLaser(Arrays.asList(cabine, moteur));

        laser.notifierObservateurs();

        laser.supprimerObservateur(cabine);

        laser.notifierObservateurs();
    }
}

