package algorithms.api.fly;

public interface FlyBehavior {
    void fly();
}
