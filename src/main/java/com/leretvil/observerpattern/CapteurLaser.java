package com.leretvil.observerpattern;

import java.util.List;

public class CapteurLaser extends Observable {
    private boolean detectecdVariation;

    public CapteurLaser(List<Observateur> observateurList) {
        super(observateurList);
    }

    public void run () {
        System.out.println("run method called");
        while (true) {
            if (detectecdVariation) {
                notifierObservateurs();
            }
        }
    }
}
