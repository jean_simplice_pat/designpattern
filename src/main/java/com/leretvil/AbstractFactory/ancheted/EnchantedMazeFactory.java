package com.leretvil.AbstractFactory.ancheted;

import com.leretvil.AbstractFactory.*;

public class EnchantedMazeFactory implements MazeFactory {
    @Override
    public Maze makeMaze() {
        return null;
    }

    @Override
    public Wall makeWall() {
        return null;
    }

    @Override
    public Room makeRoom(int n) {
        return null;
    }

    @Override
    public Door makeDoor(Room r1, Room r2) {
        return null;
    }
}
