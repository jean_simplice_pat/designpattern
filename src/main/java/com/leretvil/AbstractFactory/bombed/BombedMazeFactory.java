package com.leretvil.AbstractFactory.bombed;

import com.leretvil.AbstractFactory.*;

public class BombedMazeFactory implements MazeFactory {
    @Override
    public Maze makeMaze() {
        return null;
    }

    @Override
    public Wall makeWall() {
        return new BombedWall();
    }

    @Override
    public Room makeRoom(int n) {
        return new RoomWithBom(n);
    }

    @Override
    public Door makeDoor(Room r1, Room r2) {
        return null;
    }
}
