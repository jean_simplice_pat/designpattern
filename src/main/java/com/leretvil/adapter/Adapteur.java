package com.leretvil.adapter;

public class Adapteur implements IDeveloppeur {

    Architecte architecte;

    public Adapteur(Architecte architecte) {
        this.architecte = architecte;
    }

    @Override
    public String ecrireCode() {
        return String.format("Ecrire code || "+ architecte.ecrireAlgithme());
    }
}
