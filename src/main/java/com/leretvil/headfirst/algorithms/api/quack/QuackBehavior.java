package algorithms.api.quack;

public interface QuackBehavior {
    void quack();
}
