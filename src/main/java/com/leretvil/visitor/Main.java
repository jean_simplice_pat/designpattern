package com.leretvil.visitor;

import com.leretvil.visitor.model.Body;
import com.leretvil.visitor.model.Car;
import com.leretvil.visitor.model.Engine;
import com.leretvil.visitor.model.Wheel;

import java.util.function.Consumer;

public class Main {

    public static void main(String[] args) {
        Car car = new Car();
        Engine engine = new Engine();
        Body body = new Body();
        Wheel wheel = new Wheel();

        /*Consumer<VisitorBuilder<String>> consumer = Visitor.<String>forType(Car.class).execute(c -> "Visiting car: " + c)
                .forType(Engine.class).execute(e -> "Visiting Engine: " + e);
        */
        Consumer<VisitorBuilder<String>> consumer = VisitorV2.<Car, String>forType(Car.class).execute(c -> "Visiting car: " + c)
                .forType(Engine.class).execute(e -> "Visiting Engine: " + e)
                .forType(Wheel.class).execute(w -> "Visiting Wheel: " + w)
                .forType(Body.class).execute(b -> "Visiting Body: " + b);

        //Visitor<String> visitor = Visitor.of(consumer);
        Visitor<String> visitor = Visitor.of(consumer);

        System.out.println(visitor.visit(car));
        System.out.println(visitor.visit(engine));
        System.out.println(visitor.visit(wheel));
        System.out.println(visitor.visit(body));
/*        visitor.visit(engine);
        visitor.visit(body);
        visitor.visit(wheel);*/

    }
}
