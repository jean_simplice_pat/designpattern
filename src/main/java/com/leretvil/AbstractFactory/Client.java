package com.leretvil.AbstractFactory;

import com.leretvil.AbstractFactory.ancheted.EnchantedMazeFactory;
import com.leretvil.AbstractFactory.bombed.BombedMazeFactory;

public class Client {

    public static void main(String[] args) {
        MazeGame mazeGame = new MazeGame();

        Maze bombedMaze = mazeGame.createMaze(new BombedMazeFactory());

        Maze enchantedMaze = mazeGame.createMaze(new EnchantedMazeFactory());
    }
}
