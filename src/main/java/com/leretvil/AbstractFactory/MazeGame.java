package com.leretvil.AbstractFactory;

public class MazeGame {

    public Maze createMaze(MazeFactory factory){
        Maze aMaze = factory.makeMaze();
        Room r1 = factory.makeRoom(1);
        Room r2 = factory.makeRoom(2);
        Door aDoor = factory.makeDoor(r1, r2);

        aMaze.addRoom(r1);
        aMaze.addRoom(r2);

        r1.setSides(Direction.NORTH, factory.makeWall());
        r1.setSides(Direction.EAST, aDoor);
        r1.setSides(Direction.SOUTH, factory.makeWall());
        r1.setSides(Direction.WEST, factory.makeWall());

        r2.setSides(Direction.NORTH, factory.makeWall());
        r2.setSides(Direction.EAST, factory.makeWall());
        r2.setSides(Direction.SOUTH, factory.makeWall());
        r2.setSides(Direction.WEST, aDoor);

        return aMaze;
    }
}
