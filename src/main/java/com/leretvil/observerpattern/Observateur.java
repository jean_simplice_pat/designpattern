package com.leretvil.observerpattern;

/*Observer*/
public interface Observateur {

    public void notifier();

}
