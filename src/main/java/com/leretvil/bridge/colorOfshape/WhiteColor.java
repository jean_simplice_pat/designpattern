package com.leretvil.bridge.colorOfshape;

public class WhiteColor implements Color {
    @Override
    public void applyColor() {
        System.out.println(" White!");
    }
}
