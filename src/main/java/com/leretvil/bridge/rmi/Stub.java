package com.letetvil.bridge.rmi;

import java.rmi.Remote;

public interface Stub extends Remote {
    void doSomething();
}
