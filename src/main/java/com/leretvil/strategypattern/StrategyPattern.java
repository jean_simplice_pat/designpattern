package com.leretvil.strategypattern;

public class StrategyPattern {


    public static void main(String[] args) {

        Context context = new Context();

        context.setStrategyAddition(new StrategyAddition());

        int result = context.getStrategyAddition().execute(2, 4);

        System.out.println("Result addition = [" + result + "]");

        context.setStrategyAddition(new StrategyMultiplication());

        result = context.getStrategyAddition().execute(4,5);

        System.out.println("Result multiplication = [" + result + "]");
    }
}
