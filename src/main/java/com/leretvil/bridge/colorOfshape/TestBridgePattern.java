package com.leretvil.bridge.colorOfshape;

public class TestBridgePattern {

    public static void main(String[] args) {
        Shape rectangle = new Rectangle(new RedColor());
        rectangle.applyColor();

        Pentagon pentagon = new Pentagon(new WhiteColor());
        pentagon.applyColor();
    }
}
