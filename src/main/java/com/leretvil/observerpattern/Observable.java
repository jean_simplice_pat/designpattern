package com.leretvil.observerpattern;

import com.leretvil.observerpattern.Observateur;

import java.util.ArrayList;
import java.util.List;

public class Observable {
    List<Observateur> observateurList;

    public Observable(List<Observateur> observateurList) {
        this.observateurList = new ArrayList<>(observateurList);
    }

    public void notifierObservateurs() {
        observateurList.stream().forEach(observateur -> observateur.notifier());
    }

    void ajouterObservateur(Observateur observateur) {
        observateurList.add(observateur);
    }

    void supprimerObservateur(Observateur observateur) {
        observateurList.remove(observateur);
    }

}


