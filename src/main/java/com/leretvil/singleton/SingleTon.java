
//final class cannot be extended
public final class SingleTon {

    // volatile variable is read and write in the memory space
    // and not in the CPU cache
    // e
    private static volatile SingleTon instance = null;

    private SingleTon() {
        super();
    }

    // final method cannot be overridden by subclass
    public final static SingleTon getInstance(){
        if (instance == null) {
            synchronized (SingleTon.class) {
                if (instance == null) {
                    instance = new SingleTon();
                }
            }
        }
        return instance;
    }
}
