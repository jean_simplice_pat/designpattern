package com.leretvil.adapter;

public class Client {

    public void utiliser(IDeveloppeur developpeur) {
        System.out.println("developpeur = [" + developpeur.ecrireCode() + "]");
    }

    public static void main(String[] args) {
        DeveloppeurCpp developpeurCpp = new DeveloppeurCpp();
        Client client = new Client();
        client.utiliser(developpeurCpp);

        Architecte architecte = new Architecte();

        client.utiliser(new Adapteur(architecte));
    }

}
