package server;

import java.io.PrintWriter;

public class ConsumerServerManager extends ServerManager {

    public ConsumerServerManager(Buffer buffer, int consumerPort) {
        this.buffer = buffer;
        this.portNumber = consumerPort;
    }

    @Override
    boolean canDoProcess() {
        return !buffer.isEmpty();
    }

    @Override
    String process(String message, PrintWriter socketBufferedReader) throws InterruptedException {
        String data = buffer.consume();
        socketBufferedReader.println(data);
        return data;
    }
}
