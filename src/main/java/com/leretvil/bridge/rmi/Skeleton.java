package com.letetvil.bridge.rmi;

import java.rmi.RemoteException;
import java.rmi.server.RMIClientSocketFactory;
import java.rmi.server.RMIServerSocketFactory;
import java.rmi.server.UnicastRemoteObject;

public class Skeleton extends UnicastRemoteObject implements Stub {

    protected Skeleton(int port) throws RemoteException {
        super(port);
    }

    @Override
    public void doSomething() {

    }
}
