package algorithms.impl.quack;

import algorithms.api.quack.QuackBehavior;

public class Squeak implements QuackBehavior {
    public void quack() {
        System.out.println("Squeak (grincement)");
    }
}
