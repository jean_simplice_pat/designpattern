import java.util.*;
import java.util.stream.Collectors;

public class WordOccurrences {
    private Map<String, Integer> occurrence;
    private String[] text;
    private Set<String> words;
    public WordOccurrences(String words) {
        this.words = Arrays.stream(words.split("\\s+")).map(String::trim).collect(Collectors.toSet());
        occurrence = new HashMap<>();
    }
    public void listTheOccurrences(String text) {
        if (text == null || "".equals(text)) {
            return;
        }
        this.text = Arrays.stream(text.split("\\s+")).map(String::trim).toArray(String[]::new);
        for (String string: this.text) {
            if (words.contains(string)) {
                if(occurrence.get(string) != null) {
                    occurrence.put(string, occurrence.get(string) + 1);
                } else {
                    occurrence.put(string, 1);
                }
            }
        }
    }
    public String getOneOccurrenceIndex(int n) {
        if (occurrence.size() > n) {

        }
        return null;
    }

    public String getOneOccurrence(String string) {
        Integer count = occurrence.get(string);
        if (count != null) {
            return string +":"+count;
        }
        return string +":"+0;
    }

}
