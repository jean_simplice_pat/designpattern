package com.leretvil.bridge.colorOfshape;

public class Pentagon extends Shape {
    public Pentagon(Color color) {
        super(color);
    }

    @Override
    public void applyColor() {
        System.out.print("Pentagon of color: ");
        color.applyColor();
    }
}
