package algorithms.impl.quack;

import algorithms.api.quack.QuackBehavior;

public class Quack implements QuackBehavior {
    public void quack() {
        System.out.println("Quack");
    }
}
