package com.leretvil.prototype;

import com.leretvil.AbstractFactory.Door;
import com.leretvil.AbstractFactory.Maze;
import com.leretvil.AbstractFactory.MazeFactory;
import com.leretvil.AbstractFactory.Room;
import com.leretvil.AbstractFactory.Wall;

public interface MazePrototypeFactory  extends MazeFactory {

}
