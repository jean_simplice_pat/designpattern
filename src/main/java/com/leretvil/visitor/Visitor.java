package com.leretvil.visitor;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * The problem with this visitor is that it will save only one type in the registry
 * This issue will cause a null pointer exception when we try to visit more than one type
 * The solution is to add another interface that will save previous consumer visited: See VisitorV2 for details
 * @param <R>
 */
public interface Visitor<R> {

    static <R> Visitor<R> of(Consumer<VisitorBuilder<R>> consumer) {
        Map<Class<?>, Function<Object, R>> registry = new HashMap<>();
        consumer.accept(((type, function) -> registry.put(type, function)));
        return o -> registry.get(o.getClass()).apply(o);
    }

    R visit(Object o);

    static <R> X<R> forType(Class<?> type) {
        return () -> type;
    }

    interface X<R> {
        Class<?> type();

        default Y<R> execute(Function<Object, R> function) {
            return visitorBuilder -> visitorBuilder.register(type(), function);
        }
    }

    interface Y<R> extends Consumer<VisitorBuilder<R>> {
        default X<R> forType(Class<?> type) {
            return () -> type;
        }
    }
}
