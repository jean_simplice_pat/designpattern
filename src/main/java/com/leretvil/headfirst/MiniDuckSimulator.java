import algorithms.impl.fly.FlyRocketPowered;
import algorithms.impl.quack.Quack;
import ducks.Duck;
import ducks.DuckCall;
import ducks.MallardDuck;
import ducks.ModelDuck;

public class MiniDuckSimulator {
    public static void main(String[] args) {
        Duck mallardDuck = new MallardDuck();
        mallardDuck.performQuack();
        mallardDuck.performFly();

        Duck modelDuck = new ModelDuck();
        modelDuck.performFly();
        modelDuck.setFlyBehavior(new FlyRocketPowered());
        modelDuck.performFly();

        DuckCall duckCall = new DuckCall();
        duckCall.setQuackBehavior(new Quack());
        duckCall.performQuack();
    }
}
