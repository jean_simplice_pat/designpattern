package com.leretvil.strategypattern;

public interface Strategy {

    int execute(int a, int b);

}
